//console.log("Activity");

let pokemonTrainer = {
	name : 'Ash Ketchum',
	age : 10,
	pokemon : ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends :{
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	}
	}


	console.log(pokemonTrainer);
	console.log("Result of dot notation: ");
	console.log(pokemonTrainer.name);
	console.log("Result of square bracket notation: ");
	console.log(pokemonTrainer.pokemon);

	pokemonTrainer.talk = function(capturedPokemonName){
		this.pokemonNameOnTheArray = capturedPokemonName;
		if (pokemonTrainer.pokemon.includes(this.pokemonNameOnTheArray) === true){
		console.log(this.pokemonNameOnTheArray + "! I choose you!");
		}
		else{
			console.log("You have not capture yet that pokemon!");
		}
	}
	console.log("Result of talk method:")
	pokemonTrainer.talk('Pikachu');

	function Pokemon(name, level){
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonHealth = 2 * level;
		this.pokemonAttack = level;

		this.tackle = function(targetPokemon){
			console.log(this.pokemonName + " tackled " + targetPokemon.pokemonName);
			let damage = targetPokemon.pokemonHealth- this.pokemonAttack;
			console.log(targetPokemon.pokemonName + "'s health is now reduced to " + damage);

			damage = (damage <= 0)? true : false;
			if (damage === true){
				console.log(targetPokemon.pokemonName +" has fainted!");
			}
		}
		
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon("MewTwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);

mewtwo.tackle(geodude);

pikachu.tackle(mewtwo);